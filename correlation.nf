params.output_folder = "${baseDir}"

/* Script channels*/
extract_sample_size_script_channel = Channel.fromPath("${baseDir}/extract_sample_size.py")
generate_trait_pairs_channel = Channel.fromPath("${baseDir}/generate_trait_pairs.py")
parse_correlation_channel = Channel.fromPath("${baseDir}/parse_correlation_results.py")

ldsc_data_channel = Channel.fromPath("${baseDir}/ldsc_data/data_*.sumstats.gz")
ldsc_data_channel_bis = Channel.fromPath("${baseDir}/ldsc_data/data_*.sumstats.gz")
/*
    process related to LD-score calculation
*/


process Generate_trait_pair {
    time '1h'
    queue 'dedicated,common,ggs'
    input:
        file generate_trait_pairs_script from generate_trait_pairs_channel
        file ldsc_data from ldsc_data_channel.unique().collect()
    output:
        file "pairs_chunk_*.txt" into combi_channel mode flatten

    """
    python3 ${generate_trait_pairs_script}
    """
}

process Correlation_LDSC_data {
    memory {8.GB * task.attempt}
    time {24.h * task.attempt}
    queue 'dedicated,common,ggs'
    publishDir "${params.output_folder}/cor_data/", pattern: "*.log", mode: 'copy'
    input:
        file trait_pair from combi_channel
        file ldsc_data from ldsc_data_channel_bis.unique().collect()
    output:
        file "*.log" into cor_log_channel

    """
    export OMP_NUM_THREADS=1
    echo ${trait_pair}
    IFS=';' read -ra my_trait <<< "\$(cat ${trait_pair})"
    i=1

    for trait_p in \${my_trait[@]}
    do

        echo \$trait_p
        trait1=\$(echo \$trait_p | cut -d '.' -f1 | cut -d '_' -f2,3)
        trait2=\$(echo \$trait_p | cut -d ',' -f2 | cut -d '.' -f1 | cut -d '_' -f2,3)

        ldsc.py --rg \$trait_p --out \${trait1}-_-\${trait2} --ref-ld-chr ${baseDir}/eur_w_ld_chr/ --w-ld-chr ${baseDir}/eur_w_ld_chr/

    done
    """
}


process Correlation_matrices {

    publishDir "${params.output_folder}/Correlation_matrices/", pattern: "*.csv", mode: 'copy'
    time '1h'
    queue 'dedicated,common,ggs'
    input:
        file parsing_script from parse_correlation_channel
        file ldsc_data from cor_log_channel.collect()

    """
    python3 ${parsing_script}

    """
}
