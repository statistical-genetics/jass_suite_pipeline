import pandas as pd
import sys


f = sys.argv[1]
meta_data_loc = sys.argv[2]
d = f.split('.')[0]

D = pd.read_csv(meta_data_loc, sep="\t")
D.index = "z_"+D.Consortium+"_"+D.Outcome


Nsample = D.loc[d, "Nsample"]
print Nsample
