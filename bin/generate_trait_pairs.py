import glob
from itertools import combinations
import os

cwd = os.getcwd()
print(cwd)
L = glob.glob('{}/*.gz'.format(cwd))
L_s = [j.split('/')[-1] for j in L]
L_combi = [",".join(map(str, comb)) for comb in combinations(L_s, 2)]
print(L_s)
size_chunk = 75


N_files = len(L_combi)//size_chunk
i=0

for i in range(N_files+1):
    start = i*size_chunk
    end = (i+1)*size_chunk
    if i < N_files:
        print(";".join(L_combi[start:end]),file=open("pairs_chunk_{0}.txt".format(i), "w"))
    else:
        if len(L_combi[start:]) > 0: 
            print(";".join(L_combi[start:]),file=open("pairs_chunk_{0}.txt".format(i), "w"))
