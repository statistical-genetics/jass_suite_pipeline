#!/bin/bash -l

##############################
#       Job blueprint        #
##############################

# Give your job a name, so you can recognize it in the queue overview
#SBATCH --job-name=nextflow_jass
#SBATCH -o /pasteur/projets/policy01/PCMA/jass_analysis_pipeline/sbatch.log
#SBATCH -e /pasteur/projets/policy01/PCMA/jass_analysis_pipeline/error.log

# Define, how many nodes you need. Here, we ask for 1 node.
# Each node has 16 or 20 CPU cores.
#SBATCH --nodes=1
# You can further define the number of tasks with --ntasks-per-*
# See "man sbatch" for details. e.g. --ntasks=4 will ask for 4 cpus.

# Define, how long the job will run in real time. This is a hard cap meaning
# that if the job runs longer than what is written here, it will be
# force-stopped by the server. If you make the expected time too long, it will
# take longer for the job to start. Here, we say the job will take 5 minutes.
#              d-hh:mm:ss
#SBATCH --time=0-23:00:00

# How much memory you need.
# --mem will define memory per node and
# --mem-per-cpu will define memory per CPU/core. Choose one of those.
##SBATCH --mem-per-cpu=1500MB
#SBATCH --mem=5GB    # this one is not in effect, due to the double hash
module load java/13.0.2
module load singularity/3.7.3
module load graphviz/2.42.3
cd /pasteur/zeus/projets/p02/GGS_JASS/jass_analysis_pipeline

/pasteur/zeus/projets/p02/GGS_JASS/jass_analysis_pipeline/nextflow run jass_pipeline.nf --group './input_files/group.txt' -with-report jass_report.html -with-timeline jass_timeline.html -with-dag dag.png -c nextflow_sbatch.config
# Finish the script

exit 0
