process Impute_GWAS {
    publishDir "${params.output_folder}", pattern: "imputed_GWAS/*.txt", mode: 'copy'
    input:
        path gwas_files
        path ref_file
        path ld_file
    output:
        path "imputed_GWAS/*.txt", emit: imputed_gwas_channel
    script:
    """
    mkdir -p imputed_GWAS

    chrom=\$(echo ${gwas_files} | cut -d '_' -f4 | cut -d "." -f1)
    study=\$(echo ${gwas_files} | cut -d '_' -f2,3)

    echo \$chrom
    echo \$study

    raiss --chrom \${chrom} --gwas \${study} --ref-folder ./ --R2-threshold ${params.r2threshold} --eigen-threshold ${params.eigenthreshold} --ld-folder ./ --zscore-folder ./ --output-folder ./imputed_GWAS --ref-panel-prefix ${params.prefix_Impute_GWAS} --ref-panel-suffix ${params.suffix}.bim --minimum-ld ${params.minimumld}
    """
}

process Do_not_Impute_GWAS {
    input:
        path gwas_files from cleaned_gwas_chr_channel2.collect()
    output:
        path "clean_gwas/*.txt" into not_imputed_gwas_channel
    script:
    """
    if [ ! -d "clean_gwas" ]
    then
        mkdir clean_gwas
    fi
    cp -L *.txt ./clean_gwas

    """
}
