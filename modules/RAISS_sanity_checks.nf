
process Perf_RAISS {
    publishDir "${params.output_folder}", pattern: "imputed_GWAS/*.txt", mode: 'copy'
    publishDir "${params.output_folder}", pattern: "raiss_report/perf_report_*", mode: 'copy'
    input:
        path gwas_files
        path ref_file
        path ld_file
    output:
        path "imputed_GWAS/*.txt", emit: perf_imputed_file
        path "raiss_report/perf_report_*", emit: raiss_report
    script:
    """
    mkdir -p imputed_GWAS
    mkdir -p masked_zscore
    mkdir -p raiss_report
    chrom=\$(echo ${gwas_files} | cut -d '_' -f4 | cut -d "." -f1)
    study=\$(echo ${gwas_files} | cut -d '_' -f2,3)

    echo \$chrom
    echo \$study

    raiss --ld-folder ./ --ref-folder ./ --gwas \$study --chrom chr22 --ld-type ${params.ld_type} --ref-panel-prefix ${params.prefix_Impute_GWAS} --ref-panel-suffix ${params.suffix}.bim performance-grid-search --harmonized-folder ./ --masked-folder ./masked_zscore/ --imputed-folder ./imputed_GWAS/ --output-path ./raiss_report --eigen-ratio-grid '[${params.eigenthreshold}]' --ld-threshold-grid '[${params.r2threshold}]' --n-cpu 1
    """
}

process Put_in_harmonized_folder {
    input:
        path harmonized_files
    output:
        path "harmonized_folder/", emit: harmonized_folder
    script:
    """
    mkdir -p harmonized_folder
    mv ./z_*.txt ./harmonized_folder/
    """
}

process Put_in_imputed_folder {
    input:
        path imputed_files
    output:
        path "imputed_folder/", emit: imputed_folder 
    script:
    """
    mkdir -p imputed_folder 
    mv ./z_*.txt ./imputed_folder/
    """
}


process Sanity_Checks_RAISS {
    publishDir "${params.output_folder}", pattern: "sanity_checks_${params.ancestry}/*.txt", mode: 'copy'
    input:
        path(harmonized_files)
        path imputed_files
        val trait
        val chr_min
        val chr_max
    script:
    """
    echo $trait

    mkdir -p sanity_checks_${params.ancestry}

    raiss sanity-check --trait ${trait} --harmonized-folder ${harmonized_files} --imputed-folder ${imputed_files} --output-path ./sanity_checks_${params.ancestry}/sanity_report --chr-list "range(${chr_min},${chr_max})"
    """
}


    
// mkdir -p imputed_folder

// mv harmonized_folder/* ./
// mv imputed_folder/* ./

// for i in ./harmonized_folder/*.txt ; do mv "\$i" "\${i/harmonized_/}" ; done
// for i in ./imputed_folder/*.txt ; do mv "\$i" "\${i/imputed_/}" ; done
